# Introduction à la sécurité informatique - TP 1 

Lisez les instructions ici [droits_unix.pdf](droits_unix.pdf), et
compilez votre [rendu.md](rendu.md).


### PERSEVAL Alexandre - VALET Tristan

La commande `make`,fait à partir de l'utilisateur root, permet de créer toute l'architecture de droits d'utilisateurs et l'arborescence associée.
Les dossiers se trouvent dans "/home".
Les commandes des questions 8 et 9, `rmg` et `pwg` se trouve dans /home/admin/.

Pour supprimer tout cela, `make clean` efface tout cela.

Lire le README.md dans les dossiers question_7 et question8_9 pour lancer les scripts

La question 10 a été tenté mais pas terminé par manque de temps.
