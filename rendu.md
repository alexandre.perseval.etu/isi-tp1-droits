# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- VALET, Tristan, email: tristan.valet.etu@univ-lille.fr

- PERSEVAL, Alexandre, email: alexandre.perseval.etu@univ-lille.fr

## Question 1

Oui, l'utilisateur toto à la possibilité d'ouvrir en écriture myfile.txt grace à son groupe ubuntu. On peut voir qu'il a la permission "w" en 6ème position.

## Question 2

Lorsque l'on retire l'autorisation "x" du groupe d'un répertoire, le groupe n'aura plus le droit d'ouvrir le répertoire. Ainsi, l'utilisateur toto n'a pas le droit d'ouvrir le répertoire mydir.. 

Pareillement, pour le fichier data.txt, l'utilisateur toto n'y a pas accès étant donné qu'il n'a pas accès au répertoire parent. Lorsque l'on fait "ls -al mydir" le contenu du répertoire mydir est affiché mais les permissions sont remplacées par des "?".

## Question 3

Les différentes valeurs des ids sont les suivantes :
 * UID : 1001
 * EUID : 1002
 * GID : 1001
 * EGID : 1001

Dans le premier cas, lorsque le flag set-user-id n'est pas activé, l'utilisateur toto n'arrive pas à ouvrir le fichier mydir/mydata.txt car il n'a pas le droit. Cependant lorsque l'on active le flag, on peut constater que les ids sont les mêmes mais que cette fois, toto arrive à lire le fichier data.txt

## Question 4

Les différentes valeurs des ids sont les suivantes :
 * EUID : 1001
 * EGID : 1001

## Question 5

La commande chfn premet de modifier les informations personnelles des utilisateurs contenu dans le fichier "/etc/passwd".

La commande "ls -al /usr/bin/chfn" donne:
 * -rwsr-xr-x 1 root root 54096 Jul 27  2018 /usr/bin/chfn

On peut constater qu'il y a un petit s, signifiant que les utilisateurs ont le tag "set-user-id" indiquant qu'ils ont l'autorisation d'effectuer la commande chfn et donc de modifier le fichier associé.
Par exemple l'utilisateur toto peut modifier son numéro de téléphone grâce à la commande chfn, on peut constater que les informations sont bien mises à jour dans le fichier '/ect/passwd".


## Question 6

Les mots de passes sont stockés dans le fichier "/etc/shadow", seul l'utilisateur root à accès à ce fichier. On peut voir que les mots de passes sont chiffrés.
Cela permet d'éviter aux utilisateurs d'avoir accès aux mots de passe des autres utilisateurs, même chiffrés.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








