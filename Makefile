CC=gcc
CFLAGS=-Wall -O
CCRYPT=-lcrypt

all: init_all rmg pwg

rmg:
	$(CC) -o /home/admin/rmg question8_9/rmg.c question8_9/check.c $(CCRYPT)

pwg:
	$(CC) -o /home/admin/pwg question8_9/pwg.c question8_9/check.c $(CCRYPT)
	chmod u+s /home/admin/pwg

init_all:
	bash question7/question7_creation

clean:
	bash question7/question7_delete
