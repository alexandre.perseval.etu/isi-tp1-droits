#include<signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main() {
    //structure donnant les informations sur le serveur et sur le client
    struct sockaddr_in information_server;
    struct sockaddr_in information_client;
 
    int socketID = socket(AF_INET, SOCK_STREAM, 0);
    int connexion = 0;
    int pid;
    int id;
    char msgSend[255];
    char msgRcv[255];
 
    id=0;
    socklen_t len = sizeof(struct sockaddr_in);
 
    if (socketID == -1) {
        perror("socket");
        exit(-1);
    }
 
    memset(&information_server, 0, sizeof(struct sockaddr_in));
    information_server.sin_port = htons(4000);
    information_server.sin_family = AF_INET;
 
    /* création de la connexion*/
    if ((bind(socketID, (struct sockaddr *) &information_server, sizeof(struct sockaddr))) == -1)
    {
        perror("bind");
        exit(-1);
    }
 
    /* le serveur écoute si un client cherche à se connecter*/
    if ((listen(socketID, 5)) == -1)
    {
        perror("listen");
        exit (-1);
    }
    while (1) {
        memset(&information_client, 0, sizeof(struct sockaddr_in));
        connexion = accept(socketID, (struct sockaddr *) &information_client, &len); //le serveur accepte la connexion
 
        if (connexion == -1)
        {
            perror("accept");
            exit(-1);
        }
        id+=1;
        /* Create child process */
        pid = fork();
 
        if (pid < 0)
        {
            perror("ERROR on fork");
            exit(1);
        }
        if (pid == 0)
        {
            /* This is the client process */
            close(socketID);
            printf("Connexion acceptée de : client %i\n",id);
            sprintf(msgSend,"Connexion réussie");
            send(connexion, msgSend, strlen(msgSend), 0);
	    memset(msgSend, 0, 255);
            do
            {
                recv(connexion, msgRcv, 255, 0);
 
                if (strcmp(msgRcv, "close") == 0)    //si le client ecrit aurevoir il est deconnecté du chat
                {
                    printf ("Connexion fermée pour le client %i\n",id);
                    shutdown(socketID, SHUT_RDWR);
                    exit (0);
                }
 
                printf ("client %d : %s\n",id,msgRcv);
		memset(msgRcv, 0, 255);
                printf ("Réponse :");
                scanf("%s",msgSend);
                send(connexion, msgSend, strlen(msgSend), 0);
		memset(msgSend, 0, 255);
 
            } while(1);
        } else {
            close(connexion);
 
        }
 
    }
    return 0;
 
}
