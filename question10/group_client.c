#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <arpa/inet.h>

int main() {
 
    pid_t pid;
    int id;
    char msgSend[255];
    char msgRcv[255];
 
    struct sockaddr_in informations;  //structure donnant les informations sur le serveur
 
    informations.sin_family = AF_INET;
    informations.sin_port = htons(4000);
    informations.sin_addr.s_addr = inet_addr("127.0.0.1");
 
    int socketID = socket(AF_INET, SOCK_STREAM, 0); // creation du socket propre au client
 
    if (socketID == -1)    //test de création du socket
    {
        perror("socket");
        exit (-1);
    }
 
    if ((connect(socketID, (struct sockaddr *) &informations, sizeof(struct sockaddr_in))) == -1)   //connexion au serveur
    {
        perror("connect");
        exit (-1);
    }
 
    do
    {
        id+=1;
        printf ("moi : ");
        scanf("%s",msgSend);// le client ecrit son message
 
        if ((send(socketID, msgSend, strlen(msgSend), 0)) == -1)
            perror("send");
        memset(msgSend, 0, 255);
	recv(socketID, msgRcv, 255, 0);
        printf ("Phrase reçue : %s\n", msgRcv);
	memset(msgRcv, 0, 255);
 
    }
    while (strcmp(msgRcv, "close") != 0 && strcmp(msgSend, "close") != 0);
 
    shutdown(socketID, SHUT_RDWR);// fermeture du socket
 
    return 0;
 
}
