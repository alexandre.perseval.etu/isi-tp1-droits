Le script question7_creation sert à créer l'architecture de fichier, les utilisateurs et les groupes pour le test. (cela est appelé dans le makefile de la racine donc ne pas utiliser tel quel)

Le script question7_delete sert à supprimer les actions du script précédent. (cela est appelé dans le makefile de la racine donc ne pas utiliser tel quel)

Le script 'script_question7' est le script de test d'accessibilité, il est à placer dans un répertoire d'un utilisateur :
 Ex : /home/lambda_a/script_question7