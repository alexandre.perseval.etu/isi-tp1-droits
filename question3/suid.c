#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#define MAXCHAR 1000

int main(int argc, char *argv[])
{
    FILE *fp;
    char str[MAXCHAR];
    char* filename = argv[1];
    uid_t uid, euid;
    gid_t gid, egid;
 
    fp = fopen(filename, "r");
    if (fp == NULL){
        printf("Could not open file %s\n",filename);
    }
    else {
        printf("File %s" : \n", filename);
        while (fgets(str, MAXCHAR, fp) != NULL)
            printf("%s", str);
        fclose(fp);
        printf("\n\n");
    }
    
    uid = getuid();
    euid = geteuid();

    gid = getgid();
    egid = getegid();

    printf("UID: %dn\n", uid);
    printf("EUID: %dn\n", euid);
    printf("GID: %dn\n", gid);
    printf("EGID: %dn\n", egid);

    return 0;
}
