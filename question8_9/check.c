#include <stdio.h>
#include <stdlib.h>
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>

#include "check.h"

const char * CRYPT_KEY = "abc";

gid_t extractGroupsFile(char * fileName){
	//On récupère le groupe du fichier
	struct stat stats;
	if(stat(fileName, &stats) == -1) {
		printf("Impossible de récupérer le groupe du fichier\n");
		exit(EXIT_FAILURE);
	}
	return stats.st_gid;
}

gid_t groupIdFromName(char *name){
    struct group *grp;
    gid_t g;
    char *endptr;

    if (name == NULL || *name == '\0') 
    	    return -1;  

    g = strtol(name, &endptr, 10); 
    if (*endptr == '\0')
        return g;

    grp = getgrnam(name);
    if (grp == NULL)
        return -1;

    return grp->gr_gid;
}

int checkGroups(gid_t id) {
	int res = 0;	
	//On récupère les groupes de l'utilisateur
	struct passwd* pw = getpwuid(getuid());
	int ngroups = 0;

	getgrouplist(pw->pw_name, pw->pw_gid, NULL, &ngroups);
	__gid_t groups[ngroups];

	getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);

	for (int i = 0; i < ngroups; i++){
    		struct group* gr = getgrgid(groups[i]);
		if(gr->gr_gid == id) {
			res = 1;
		}
	}

	return res;
}

int checkPassword(char * mdp) {
	int res = 0;
	FILE* filePointer;
	int bufferLength = 255;
	char search[bufferLength] ;
	char * cryptMdp = "x";

	strcpy(search, getenv("USER"));

	if (strcmp(mdp, cryptMdp)) {
		cryptMdp = crypt(mdp, CRYPT_KEY);
	}
	
	strcat(search,":");
	strcat(search,cryptMdp);

	//printf("search : %s\n",search);
	char line[bufferLength];
	filePointer = fopen("/home/admin/passwd", "r");
	while(fgets(line, bufferLength, filePointer))
	{
		char *ptr = strstr(line, search);
		//printf("debug %s %s %s\n", ptr, line, search);

		if (ptr != NULL) 
		{
			res = 1;
			break;
		}
	}

	//printf("%s\n",getenv("USER"));
	fclose(filePointer);

	return res;
}

int havePassword(){
	return ! checkPassword("x");
}

int changePassword(char * newMdp){
	int res = 0;
	FILE* fp;
       	FILE* fpTmp;

	int bufferLength = 255;
	char line[bufferLength];
	char search[bufferLength];
	char * cryptMdp = crypt(newMdp, CRYPT_KEY);

	strcpy(search, getenv("USER"));
	strcat(search,":");

	fp = fopen("/home/admin/passwd", "r");
	fpTmp = fopen("/home/admin/passwdTMP", "w");

	while(fgets(line, bufferLength, fp))
	{
		char *ptr = strstr(line, search);
		//printf("debug %s %s %s\n", ptr, line, search);

		if (ptr != NULL) 
		{
			res = 1;
			strcat(search, cryptMdp);
			strcat(search,"\n");
			fputs(search, fpTmp);
			//printf("debug\n");
		} else {
			fputs(line,fpTmp);
		}
	}
		
	fclose(fp);
	fclose(fpTmp);

	remove("/home/admin/passwd");
	rename("/home/admin/passwdTMP", "/home/admin/passwd");

	return res;
} 
