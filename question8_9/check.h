#ifndef CHECK_H
#define CHECK_H 

#include <sys/types.h>

gid_t extractGroupsFile(char * fileName);
gid_t groupIdFromName(char * name);
int checkGroups(gid_t id);

int checkPassword(char * mdp);
int havePassword();
int changePassword(char * newMdp);

#endif
