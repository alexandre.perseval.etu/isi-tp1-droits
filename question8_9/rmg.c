#include <stdio.h>
#include "check.h"

int main(int argc, char* argv[]){
	if(argc <= 1) {
		printf("Merci de passer un fichier en paramètre\n");
	} else {
		char mdp[25];
		printf("Fichier à supprimer : %s\n", argv[1]);		
		if(checkGroups(extractGroupsFile(argv[1]))) {
			printf("Le fichier appartient bien à votre groupe.\n");
			if (havePassword()){
				printf("Veuillez saisir votre mot de passe :\n");
				scanf("%s", &mdp);

				if(! checkPassword(mdp)) {
					printf("Mot de passe incorrect.\n");
					return 0;
				}
				printf("Mot de passe correct...\n\n");
			}
			printf("\tSuppression du fichier %s\n",argv[1]);
			if(remove(argv[1]) == 0) {
				printf("\tFichier supprimé avec succès.\n");
			} else {
				printf("\tVous n'avez pas les droits sur ce fichier.\n");
			}
			
		} else {
			printf("Le fichier n'appartient pas au même groupe que vous.\n");
		}
	}
}
