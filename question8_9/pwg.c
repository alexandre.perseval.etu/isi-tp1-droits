#include <stdio.h>
#include <sys/types.h>
#include <string.h>

#include "check.h"

int main(int argc, char* argv[]){
	char mdp [25];
	char tmp [25];
	
	gid_t ga = groupIdFromName("groupe_a");
	gid_t gb = groupIdFromName("groupe_b");

	if(checkGroups(ga) || checkGroups(gb)){

		if(havePassword()) {

			printf("Veuillez saisir votre mot de passe :\n");
			scanf("%s", &mdp);
			if (checkPassword(mdp)){
				printf("Mot de passe correct.\n\n");
			} else {
				printf("Mot de passe incorrect.\n");
				return 0;
			}
		}

		while (strcmp(mdp, tmp)){
			printf("Nouveau mot de passe: ");
			scanf("%s",&mdp);
			printf("Confirmer mot de passe: ");
			scanf("%s",&tmp);
		}

		if (changePassword(mdp)){
			printf("Mot de passe modifié.\n");
		} else {
			printf("Une erreur est survenu.\nLe mot de passe n'a pas été modifié.\n");
		}

		
	} else {
		printf("Vous ne faites pas partir du bon groupe d'utilisateur.\n");
	}

	return 0;

}
